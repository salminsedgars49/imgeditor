import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Button, Switch, Tooltip } from 'antd';
import i18n from 'i18next';

import ImageMapList from './ImageMapList';
import CommonButton from '../common/CommonButton';
import { FlexBox, FlexItem } from '../flex';
import Icon from '../icon/Icon';


class ImageMapFooterToolbar extends Component {
    static propTypes = {
        canvasRef: PropTypes.any,
        preview: PropTypes.bool,
        onChangePreview: PropTypes.func,
        zoomRatio: PropTypes.number,
        selectedItem: PropTypes.object,
        onLayerSelect: PropTypes.func,
    }

    state = {
        interactionMode: 'selection',
        showLayerControl: false,
    }

    componentDidMount() {
        const { canvasRef } = this.props;
        this.waitForCanvasRender(canvasRef);
    }

    componentWillUnmount() {
        const { canvasRef } = this.props;
        this.detachEventListener(canvasRef);
    }

    waitForCanvasRender = (canvas) => {
        setTimeout(() => {
            if (canvas) {
                this.attachEventListener(canvas);
                return;
            }
            const { canvasRef } = this.props;
            this.waitForCanvasRender(canvasRef);
        }, 5);
    };

    attachEventListener = (canvasRef) => {
        canvasRef.canvas.wrapperEl.addEventListener('keydown', this.events.keydown, false);
    }

    detachEventListener = (canvasRef) => {
        canvasRef.canvas.wrapperEl.removeEventListener('keydown', this.events.keydown);
    }

    /* eslint-disable react/sort-comp, react/prop-types */
    handlers = {
        selection: () => {
            this.props.canvasRef.handler.modeHandler.selection((obj) => {
                return {
                    selectable: obj.superType !== 'port',
                    evented: true,
                };
            });
            this.setState({ interactionMode: 'selection' });
        },
        grab: () => {
            this.props.canvasRef.handler.modeHandler.grab();
            this.setState({ interactionMode: 'grab' });
        },
    }

    events = {
        keydown: (e) => {
            if (this.props.canvasRef.canvas.wrapperEl !== document.activeElement) {
                return false;
            }
            if (e.keyCode === 81) {
                this.handlers.selection();
            } else if (e.keyCode === 87) {
                this.handlers.grab();
            }
        },
    }

    controlHandlers = {
        onLayerSelect: () => {
            const { onLayerSelect } = this.props;
            onLayerSelect();
        }
    }

    render() {
        const { canvasRef, preview, zoomRatio, onChangePreview, selectedItem } = this.props;
        const { interactionMode } = this.state;
        const { selection, grab } = this.handlers;
        if (!canvasRef) {
            return null;
        }
        const zoomValue = parseInt((zoomRatio * 100).toFixed(2), 10);
        const isCropping = canvasRef ? canvasRef.handler.interactionMode === 'crop' : false;

        return (
            <React.Fragment>
                <Row style={{ width: '100%' }}>
                    <Col span={8}>
                        <FlexBox justifyContent="flex-start">
                            <FlexItem className="rde-canvas-toolbar rde-canvas-toolbar-list">
                                <CommonButton
                                    className="rde-action-btn"
                                    shape="circle"
                                    icon="layer-group"
                                    tooltipTitle={i18n.t('action.canvas-list')}
                                    onClick={this.controlHandlers.onLayerSelect}
                                />
                                {/*<div className="rde-canvas-list">
                                    <ImageMapList canvasRef={canvasRef} selectedItem={selectedItem} />
                                </div>*/}
                            </FlexItem>
                            <FlexItem>
                                <CommonButton
                                    type={interactionMode === 'selection' ? 'primary' : 'default'}
                                    className="rde-action-btn"
                                    shape="circle"
                                    onClick={() => { selection(); }}
                                    icon="mouse-pointer"
                                    tooltipTitle={i18n.t('action.selection')}
                                />
                            </FlexItem>
                            <FlexItem>
                                <CommonButton
                                    type={interactionMode === 'grab' ? 'primary' : 'default'}
                                    className="rde-action-btn"
                                    shape="circle"
                                    onClick={() => { grab(); }}
                                    tooltipTitle={i18n.t('action.grab')}
                                    icon="hand-rock"
                                />
                            </FlexItem>
                            <FlexItem>
                                <CommonButton
                                    onClick={() => { canvasRef.handler.zoomHandler.zoomToFit(); }}
                                    className="rde-action-btn"
                                    shape="circle"
                                    tooltipTitle={i18n.t('action.fit')}
                                    icon="expand"
                                />
                            </FlexItem>
                        </FlexBox>
                    </Col>

                    <Col span={8}>
                        <FlexBox justifyContent="center">
                            <FlexItem className="rde-editor-footer-toolbar-zoom">
                                <Button.Group>
                                    <CommonButton
                                        className="rde-action-btn"
                                        onClick={() => { canvasRef.handler.zoomHandler.zoomOut(); }}
                                        icon="minus"
                                        tooltipTitle={i18n.t('action.zoom-out')}
                                    />
                                    <CommonButton
                                        className="rde-action-btn rde-editor-footer-toolbar-zoom-value"
                                        onClick={() => { canvasRef.handler.zoomHandler.zoomOneToOne(); }}
                                        tooltipTitle={i18n.t('action.one-to-one')}
                                    >
                                        {`${zoomValue}%`}
                                    </CommonButton>
                                    <CommonButton
                                        className="rde-action-btn"
                                        onClick={() => { canvasRef.handler.zoomHandler.zoomIn(); }}
                                        icon="plus"
                                        tooltipTitle={i18n.t('action.zoom-in')}
                                    />
                                </Button.Group>
                            </FlexItem>
                        </FlexBox>
                    </Col>

                    <Col span={8}>
                        <FlexBox justifyContent="flex-end">
                            <FlexItem>
                                <CommonButton
                                    className="rde-action-btn"
                                    shape="circle"
                                    disabled={isCropping}
                                    onClick={() => canvasRef.handler.remove()}
                                    icon="trash"
                                    tooltipTitle={i18n.t('action.delete')}
                                />
                            </FlexItem>
                            <FlexItem>
                                <CommonButton
                                    className="rde-action-btn"
                                    shape="circle"
                                    disabled={isCropping || (canvasRef && !canvasRef.handler.transactionHandler.undos.length)}
                                    onClick={() => canvasRef.handler.transactionHandler.undo()}
                                    icon="undo-alt"
                                    tooltipTitle={i18n.t('action.undo')}
                                />
                            </FlexItem>
                            <FlexItem>
                                <CommonButton
                                    className="rde-action-btn"
                                    shape="circle"
                                    disabled={isCropping || (canvasRef && !canvasRef.handler.transactionHandler.redos.length)}
                                    onClick={() => canvasRef.handler.transactionHandler.redo()}
                                    icon="redo-alt"
                                    tooltipTitle={i18n.t('action.redo')}
                                />
                            </FlexItem>
                            <FlexItem className="rde-editor-footer-toolbar-preview">
                                <Tooltip title={i18n.t('action.preview')}>
                                    <Switch checked={preview} onChange={onChangePreview} />
                                </Tooltip>
                            </FlexItem>
                        </FlexBox>
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}

export default ImageMapFooterToolbar;

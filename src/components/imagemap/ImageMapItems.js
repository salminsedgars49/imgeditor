import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Tabs, Collapse, notification, Input, message, Button, Row, Col } from 'antd';
import uuid from 'uuid/v4';
import classnames from 'classnames';
import i18n from 'i18next';

import { FlexBox } from '../flex';
import Icon from '../icon/Icon';
import Scrollbar from '../common/Scrollbar';
import CommonButton from '../common/CommonButton';
import { SVGModal } from '../common';

import MapProperties from './properties/MapProperties';
import { GlobalConfig } from '../../config'


notification.config({
    top: 80,
    duration: 2,
});

const { Panel } = Collapse;


class ImageMapItems extends Component {
    static propTypes = {
        canvasRef: PropTypes.any,
        descriptors: PropTypes.object,
    }

    state = {
        activeKey: 'image',
        collapse: false,
        textSearch: '',
        descriptors: {},
        filteredDescriptors: [],
        svgModalVisible: false,
    }

    componentDidMount() {
        const { canvasRef } = this.props;
        this.waitForCanvasRender(canvasRef);
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (JSON.stringify(this.props.descriptors) !== JSON.stringify(nextProps.descriptors)) {
            const descriptors = Object.keys(nextProps.descriptors).reduce((prev, key) => {
                return prev.concat(nextProps.descriptors[key]);
            }, []);
            this.setState({
                descriptors,
            });
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (JSON.stringify(this.state.descriptors) !== JSON.stringify(nextState.descriptors)) {
            return true;
        } else if (JSON.stringify(this.state.filteredDescriptors) !== JSON.stringify(nextState.filteredDescriptors)) {
            return true;
        } else if (this.state.textSearch !== nextState.textSearch) {
            return true;
        } else if (JSON.stringify(this.state.activeKey) !== JSON.stringify(nextState.activeKey)) {
            return true;
        } else if (this.state.collapse !== nextState.collapse) {
            return true;
        } else if (this.state.svgModalVisible !== nextState.svgModalVisible) {
            return true;
        }
        return false;
    }

    componentWillUnmount() {
        const { canvasRef } = this.props;
        this.detachEventListener(canvasRef);
    }

    waitForCanvasRender = (canvas) => {
        setTimeout(() => {
            if (canvas) {
                this.attachEventListener(canvas);
                return;
            }
            const { canvasRef } = this.props;
            this.waitForCanvasRender(canvasRef);
        }, 5);
    };

    attachEventListener = (canvas) => {
        canvas.canvas.wrapperEl.addEventListener('dragenter', this.events.onDragEnter, false);
        canvas.canvas.wrapperEl.addEventListener('dragover', this.events.onDragOver, false);
        canvas.canvas.wrapperEl.addEventListener('dragleave', this.events.onDragLeave, false);
        canvas.canvas.wrapperEl.addEventListener('drop', this.events.onDrop, false);
    }

    detachEventListener = (canvas) => {
        canvas.canvas.wrapperEl.removeEventListener('dragenter', this.events.onDragEnter);
        canvas.canvas.wrapperEl.removeEventListener('dragover', this.events.onDragOver);
        canvas.canvas.wrapperEl.removeEventListener('dragleave', this.events.onDragLeave);
        canvas.canvas.wrapperEl.removeEventListener('drop', this.events.onDrop);
    }

    /* eslint-disable react/sort-comp, react/prop-types */
    handlers = {
        onAddItem: (item, centered) => {
            const { canvasRef } = this.props;
            if (canvasRef.handler.workarea.layout === 'responsive') {
                if (!canvasRef.handler.workarea.isElement) {
                    notification.warn({
                        message: 'Please your select background image',
                    });
                    return;
                }
            }
            if (canvasRef.handler.interactionMode === 'polygon') {
                message.info('Already drawing');
                return;
            }
            const id = uuid();
            const option = Object.assign({}, item.option, { id });
            if (item.option.type === 'svg' && item.type === 'default') {
                this.handlers.onSVGModalVisible(item.option);
                return;
            }
            canvasRef.handler.add(option, centered);
        },
        onAddSVG: (option, centered) => {
            const { canvasRef } = this.props;
            canvasRef.handler.add({ ...option, type: 'svg', id: uuid(), name: 'New SVG' }, centered);
            this.handlers.onSVGModalVisible();
        },
        onDrawingItem: (item) => {
            const { canvasRef } = this.props;
            if (canvasRef.handler.workarea.layout === 'responsive') {
                if (!canvasRef.handler.workarea.isElement) {
                    notification.warn({
                        message: 'Please your select background image',
                    });
                    return;
                }
            }
            if (canvasRef.handler.interactionMode === 'polygon') {
                message.info('Already drawing');
                return;
            }
            if (item.option.type === 'line') {
                canvasRef.handler.drawingHandler.line.init();
            } else if (item.option.type === 'arrow') {
                canvasRef.handler.drawingHandler.arrow.init();
            } else {
                canvasRef.handler.drawingHandler.polygon.init();
            }
        },
        onChangeActiveKey: (activeKey) => {
            this.setState({
                activeKey,
            });
        },
        onChangeTab: (activeKey) => {
            this.setState({
                activeKey,
            });
        },
        onCollapse: () => {
            this.setState({
                collapse: !this.state.collapse,
            });
        },
        onSearchNode: (e) => {
            const filteredDescriptors = this.handlers.transformList().filter(descriptor => descriptor.name.toLowerCase().includes(e.target.value.toLowerCase()));
            this.setState({
                textSearch: e.target.value,
                filteredDescriptors,
            });
        },
        transformList: () => {
            return Object.values(this.props.descriptors).reduce((prev, curr) => prev.concat(curr), []);
        },
        onSVGModalVisible: () => {
            this.setState((prevState) => {
                return {
                    svgModalVisible: !prevState.svgModalVisible,
                };
            });
        },
    }

    events = {
        onDragStart: (e, item) => {
            this.item = item;
            const { target } = e;
            target.classList.add('dragging');
        },
        onDragOver: (e) => {
            if (e.preventDefault) {
                e.preventDefault();
            }
            e.dataTransfer.dropEffect = 'copy';
            return false;
        },
        onDragEnter: (e) => {
            const { target } = e;
            target.classList.add('over');
        },
        onDragLeave: (e) => {
            const { target } = e;
            target.classList.remove('over');
        },
        onDrop: (e) => {
            e = e || window.event;
            if (e.preventDefault) {
                e.preventDefault();
            }
            if (e.stopPropagation) {
                e.stopPropagation();
            }
            const { layerX, layerY } = e;
            const dt = e.dataTransfer;
            if (dt.types.length && dt.types[0] === 'Files') {
                const { files } = dt;
                Array.from(files).forEach((file) => {
                    file.uid = uuid();
                    const { type } = file;
                    if (type === 'image/png' || type === 'image/jpeg' || type === 'image/jpg') {
                        const item = {
                            option: {
                                type: 'image',
                                file,
                                left: layerX,
                                top: layerY,
                            },
                        };
                        this.handlers.onAddItem(item, false);
                    } else {
                        notification.warn({
                            message: 'Not supported file type',
                        });
                    }
                });
                return false;
            }
            const option = Object.assign({}, this.item.option, { left: layerX, top: layerY });
            const newItem = Object.assign({}, this.item, { option });
            this.handlers.onAddItem(newItem, false);
            return false;
        },
        onDragEnd: (e) => {
            this.item = null;
            e.target.classList.remove('dragging');
        },
    }

    imageOpenHandlers = {
        onClickImage: (src) => {
            var imageItem = {
                name: "Image",
                type: "image",
                option: {
                    type: "image",
                    name: "New image",
                    src: src
                }
            };

            this.handlers.onAddItem(imageItem);
        },

        onChangeFile: (event) => {
            event.stopPropagation();
            event.preventDefault();
            var file = event.target.files[0];

            const isLimit = file.size / 1024 / 1024 < GlobalConfig.imgSizeLimit;

            if (!isLimit) {
                // message.error(`Limited to ${limit}MB or less`);
                // return false;
            }

            var imageItem = {
                name: "Image",
                type: "image",
                option: {
                    type: "image",
                    name: "New image",
                    file: file
                }
            };

            this.handlers.onAddItem(imageItem);
        }
    }

    imageGallary = (image, index) => (
        <Col
            className="gallary-col"
            md={12}
            key={index}
            onClick={() => this.imageOpenHandlers.onClickImage(image)}
        >
            <div className="gallary-item" style={{ backgroundImage: `url(${image})` }}></div>
        </Col>
    )

    renderImageOpenTab = () => (
        <div className="rde-image-open-tab">
            <Row gutter={8}>
                <Col md={20}>
                    <Button type="primary" onClick={() => { this.imageUpload.click() }} >
                        Computer
                    </Button>
                </Col>
                <Col md={4}>
                    <Button type="primary" onClick={() => { this.imageUpload.click() }} style={{ padding: 0 }} >
                        <Icon name="clone" />
                    </Button>
                </Col>
            </Row>

            <Row gutter={4} className="rde-image-open-tab__gallery">
                { GlobalConfig.galleryImages.map((image, index) => this.imageGallary(image, index)) }
            </Row>

            <input
                id="imageUpload"
                type="file"
                accept="image/*"
                ref={ (ref) => this.imageUpload = ref }
                style={{display: 'none'}}
                onChange={ this.imageOpenHandlers.onChangeFile.bind(this) }
            />
        </div>
    )

    renderItems = items => (
        <FlexBox flexWrap="wrap" flexDirection="column" style={{ width: '100%' }}>
            {items.map(item => this.renderItem(item))}
        </FlexBox>
    )

    renderItem = (item, centered) => (
        item.type === 'drawing' ? (
            <div
                key={item.name}
                draggable
                onClick={e => this.handlers.onDrawingItem(item)}
                className="rde-editor-items-item"
                style={{ justifyContent: this.state.collapse ? 'center' : null }}
            >
                <span className="rde-editor-items-item-icon">
                    <Icon name={item.icon.name} prefix={item.icon.prefix} style={item.icon.style} />
                </span>
                {
                    this.state.collapse ? null : (
                        <div className="rde-editor-items-item-text">
                            {item.name}
                        </div>
                    )
                }
            </div>
        ) : (
            <div
                key={item.name}
                draggable
                onClick={e => this.handlers.onAddItem(item, centered)}
                onDragStart={e => this.events.onDragStart(e, item)}
                onDragEnd={e => this.events.onDragEnd(e, item)}
                className="rde-editor-items-item"
                style={{ justifyContent: this.state.collapse ? 'center' : null }}
            >
                <span className="rde-editor-items-item-icon">
                    <Icon name={item.icon.name} prefix={item.icon.prefix} style={item.icon.style} />
                </span>
                {
                    this.state.collapse ? null : (
                        <div className="rde-editor-items-item-text">
                            {item.name}
                        </div>
                    )
                }
            </div>
        )
    )

    render() {
        const { descriptors, canvasRef, onChange } = this.props;
        const {
            collapse,
            textSearch,
            filteredDescriptors,
            activeKey,
            svgModalVisible,
            svgOption,
        } = this.state;
        const className = classnames('rde-editor-configurations', {
            minimize: collapse,
        });
        const { onChangeTab, onCollapse } = this.handlers;

        const renderTabs = descriptors.map((item) => (
            <Tabs.TabPane
                tab={<Icon name={item.icon.name} />}
                key={item.name}
            >
                <div className="rde-tools-content">
                    <div className="rde-tools-title">{item.name}</div>
                    { item.name == 'image' ? this.renderImageOpenTab() : (
                        <Collapse defaultActiveKey={['1']} className="rde-tools-collapse">
                            <Panel header="Essentials" key="1">
                                {this.renderItems(item.child)}
                            </Panel>
                        </Collapse>
                    )}
                </div>
            </Tabs.TabPane>
        ))

        return (
            <div className={className}>
                <CommonButton
                    className="rde-action-btn"
                    shape="circle"
                    icon={collapse ? 'angle-double-right' : 'angle-double-left'}
                    onClick={onCollapse}
                    style={{ position: 'absolute', top: 16, right: 10, zIndex: 1000, color: '#b6b6b6' }}
                />
                <Tabs
                    tabPosition="left"
                    style={{ height: '100%' }}
                    activeKey={activeKey}
                    onChange={onChangeTab}
                    tabBarStyle={{ marginTop: 60 }}
                    className="rde-tools"
                >
                    {renderTabs}
                </Tabs>
            </div>
        );
    }
}

export default ImageMapItems;

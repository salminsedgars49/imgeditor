import React, { Component } from 'react';
import { FlexBox, FlexItem } from '../flex';

class ImageMapTitle extends Component {
    render() {
        const { title, content, action, children } = this.props;
        return children || (
            <FlexBox className="rde-content-layout-title" alignItems="center" flexWrap="wrap">
                <FlexItem flex="auto">
                    <FlexBox className="rde-content-layout-title-action" justifyContent="center" alignItems="center">
                        {action}
                    </FlexBox>
                </FlexItem>
            </FlexBox>
        );
    }
}

export default ImageMapTitle;

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Row, Col } from 'antd';
import classnames from 'classnames';

import Icon from '../icon/Icon';
import Canvas from '../canvas/Canvas';

import { GlobalConfig } from '../../config'


class ImageOpener extends Component {
    static propTypes = {
        showImageOpen: PropTypes.bool,
        onChangeOpener: PropTypes.func,
        onChangeFile: PropTypes.func,
        onChangeFileSrc: PropTypes.func,
    }

    _onClickImage = (src) => {
        console.log(`image selected: ${src}`);
        const { onChangeFileSrc, onChangeOpener } = this.props;
        onChangeFileSrc(src);
        onChangeOpener(false);
    }

    _onChangeFile = (event) => {
        const { onChangeFile, onChangeOpener } = this.props;
        onChangeFile(event);
        onChangeOpener(false);
    }

    render() {
        const { showImageOpen } = this.props;
        const startImages = GlobalConfig.startImages;
        const openerClassName = classnames('rde-image-opener', { showImageOpen });

        const imageGallary = startImages.map((image, index) => (
            <Col className="gutter-row gallary-item" key={index} md={8}>
                <img src={image.url} alt={image.name} onClick={() => this._onClickImage(image.url)} />
            </Col>
        ))

        return (
            <div className={openerClassName}>
                <div ref={(c) => { this.container = c; }} style={{ overvlow: 'hidden', display: 'flex', flex: '1', height: '100%' }}>
                    <div className="image-box">
                        <h3>Please open a photo or project to<br />get started</h3>
                        <div className="image-box__upload">
                            <Button type="primary" onClick={() => { this.imageUpload.click() }} >
                                <Icon name="upload" style={{ marginRight: 10 }} />
                                Select File
                            </Button>
                        </div>
                        <h3>or use one of ours</h3>
                        <Row gutter={16}>{imageGallary}</Row>
                    </div>

                    <input
                        id="imageUpload"
                        type="file"
                        accept="image/*"
                        ref={ (ref) => this.imageUpload = ref }
                        style={{display: 'none'}}
                        onChange={ this._onChangeFile.bind(this) }
                    />

                </div>
            </div>
        );
    }
}

export default ImageOpener;

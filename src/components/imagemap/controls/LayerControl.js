import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Popover, Icon } from 'antd';
import i18n from 'i18next';

import CommonButton from '../../common/CommonButton';
import Scrollbar from '../../common/Scrollbar';
import Draggable from 'react-draggable';
import ImageMapList from '../ImageMapList';


class LayerControl extends Component {

    state = {
        helpVisible: false,
    }

    helpHandlers = {
        onHide: () => {
            this.setState({
                helpVisible: false,
            });
        },
        onVisibleChange: (visible) => {
            this.setState({ helpVisible: visible });
        }
    }

    handlers = {
        onClose: () => {
            const { onClose } = this.props;
            onClose();
        }
    }

    render() {
        const { canvasRef, selectedItem } = this.props;
        const { showControl } = this.state;

        const helpContent = (
            <div className="rde-help-content">
                <div className="rde-help-content-text">
                    Groups allow you move, align, and resize multiple layers at the same time. To create a group, select your layers then right click and select "Create Group".
                </div>
                <div className="rde-help-content-action">
                    <a onClick={this.helpHandlers.onHide}>OK, GOT IT!</a>
                </div>
            </div>
        );

        return (
            <Draggable
                defaultPosition={{x: 360, y: 400}}
            >
                <div className="rde-control">
                    <div className="rde-card">
                        <div className="rde-card-header">
                            Layers
                            <div className="rde-card-header-actions">
                                <Popover
                                    content={helpContent}
                                    title="Layers"
                                    trigger="click"
                                    visible={this.state.helpVisible}
                                    onVisibleChange={this.helpHandlers.onVisibleChange}
                                >
                                    <CommonButton
                                        className="rde-action-btn"
                                        shape="circle"
                                        tooltipTitle={i18n.t('common.help')}
                                    >
                                        <Icon type="question-circle" />
                                    </CommonButton>
                                </Popover>
                                <CommonButton
                                    className="rde-action-btn"
                                    shape="circle"
                                    tooltipTitle={i18n.t('common.close')}
                                    onClick={this.handlers.onClose}
                                >
                                    <Icon type="close" />
                                </CommonButton>
                            </div>
                        </div>
                        <div className="rde-card-body">
                            <ImageMapList canvasRef={canvasRef} selectedItem={selectedItem} />
                        </div>
                    </div>
                </div>
            </Draggable>
        );

    }
}

LayerControl.propTypes = {
    canvasRef: PropTypes.any,
    onClose: PropTypes.func,
    selectedItem: PropTypes.object,
}

export default LayerControl;

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Popover, Icon } from 'antd';
import i18n from 'i18next';
import classnames from 'classnames';

import CommonButton from '../../common/CommonButton';
import Scrollbar from '../../common/Scrollbar';
import PropertyDefinition from '../properties/PropertyDefinition';
import NodeProperties from '../properties/NodeProperties';


class PropertyControl extends Component {

    state = {
        helpVisible: false,
    }

    helpHandlers = {
        onHide: () => {
            this.setState({
                helpVisible: false,
            });
        },
        onVisibleChange: (visible) => {
            this.setState({ helpVisible: visible });
        }
    }

    handlers = {
        onClose: () => {
            const { onCollapse } = this.props;
            onCollapse(false);
        }
    }

    render() {
        const {
            canvasRef,
            selectedItem,
            collapsedControl,
            onChange,
        } = this.props;
        const { showControl } = this.state;

        const helpContent = (
            <div className="rde-help-content">
                <div className="rde-help-content-text">
                    Groups allow you move, align, and resize multiple layers at the same time. To create a group, select your layers then right click and select "Create Group".
                </div>
                <div className="rde-help-content-action">
                    <a onClick={this.helpHandlers.onHide}>OK, GOT IT!</a>
                </div>
            </div>
        );

        const previewClassName = classnames('propery-control', { collapsedControl });

        return (
            <div className={previewClassName}>
                <div className="rde-card">
                    <div className="rde-card-header">
                        Properties
                        <div className="rde-card-header-actions">
                            <Popover
                                content={helpContent}
                                title="Properties"
                                trigger="click"
                                visible={this.state.helpVisible}
                                onVisibleChange={this.helpHandlers.onVisibleChange}
                            >
                                <CommonButton
                                    className="rde-action-btn"
                                    shape="circle"
                                    tooltipTitle={i18n.t('common.help')}
                                >
                                    <Icon type="question-circle" />
                                </CommonButton>
                            </Popover>
                            <CommonButton
                                className="rde-action-btn"
                                shape="circle"
                                tooltipTitle={i18n.t('common.close')}
                                onClick={this.handlers.onClose}
                            >
                                <Icon type="close" />
                            </CommonButton>
                        </div>
                    </div>
                    <div className="rde-card-body" style={{ height: 350 }}>
                        <Scrollbar>
                            <NodeProperties onChange={onChange} selectedItem={selectedItem} canvasRef={canvasRef} />
                        </Scrollbar>
                    </div>
                </div>
            </div>
        );

    }
}

PropertyControl.propTypes = {
    canvasRef: PropTypes.any,
    onCollapse: PropTypes.func,
    selectedItem: PropTypes.object,
    onChange: PropTypes.func,
}

export default PropertyControl;

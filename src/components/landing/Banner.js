import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { FlexBox, FlexItem } from '../flex';
import { Button } from 'antd';

import { AppImages } from '../../assets'
import { linkConfig } from '../../config'


class Banner extends Component {

    render() {
        return (
            <div className="landing-banner">
                <div className="landing-banner__container">
                    <h1>Online Image Editor</h1>
                    <h2>Photo Editing and Graphic Design Made for Everyone</h2>
                    <FlexBox flexWrap="wrap" flex="1" alignItems="center">
                        <FlexBox flex="1 1 auto" justifyContent="center">
                            <FlexItem>
                                <Button
                                    className="rde-action-btn btn--filled"
                                    value="large"
                                    type="primary"
                                    href={linkConfig.editor}
                                >
                                    Get Started
                                </Button>
                            </FlexItem>
                            <FlexItem>
                                <Button
                                    className="rde-action-btn btn--transparent"
                                    href={linkConfig.login}
                                >
                                    Sign In
                                </Button>
                            </FlexItem>
                        </FlexBox>
                    </FlexBox>

                    {/*<FlexBox className="landing-banner__images" flexWrap="wrap" flex="1" alignItems="center" justifyContent="space-between">
                        <div className="landing-banner__images__m1">
                            <img src={AppImages.features_touchup} alt="features-touchup" />
                        </div>
                        <div className="landing-banner__images__m2">
                            <img src={AppImages.features_transpbg} alt="features-transpbg" />
                        </div>
                        <div className="landing-banner__images__m3">
                            <img src={AppImages.features_batch_process} alt="features-batch-process" />
                        </div>
                        <div className="landing-banner__images__m4">
                            <img src={AppImages.features_photoart} alt="features-photoart" />
                        </div>
                    </FlexBox>*/}
                </div>
            </div>
        );
    }
}

export default Banner;

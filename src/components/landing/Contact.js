import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { FlexBox, FlexItem } from '../flex';
import { Typography, Form, Input, Button } from 'antd';

const { Title } = Typography;
const { TextArea } = Input;


class Contact extends Component {

    onFinish = (values) => {
        console.log('Success:', values);
    }

    onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    }

    render() {
        const layout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 },
        };

        const tailLayout = {
            wrapperCol: { offset: 20, span: 4 },
        };

        return (
            <div className="landing-contact">
                <div className="landing-contact__container">
                    <Title>CONTACT US</Title>
                    <Form
                        name="basic"
                        initialValues={{ remember: true }}
                    >
                        <FlexBox flexWrap="wrap" flex="1" justifyContent="center" className="landing-contact__form">
                            <FlexItem>
                                <Form.Item
                                    name="role"
                                    rules={[{ required: true, message: 'Please input your role!' }]}
                                >
                                    <Input placeholder="Who are you? *" />
                                </Form.Item>

                                <Form.Item
                                    name="email"
                                    rules={[{ required: true, message: 'Please input your email!' }]}
                                >
                                    <Input placeholder="Your email? *" />
                                </Form.Item>
                            </FlexItem>
                            <FlexItem>
                                <Form.Item
                                    name="content"
                                    rules={[{ required: true, message: 'Please input your messages!' }]}
                                >
                                    <TextArea rows={4} placeholder="Messages" />
                                </Form.Item>
                            </FlexItem>
                        </FlexBox>

                        <Form.Item style={{ textAlign: 'right' }}>
                            <Button type="primary" htmlType="submit">
                                SEND MESSAGE
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        );
    }
}

export default Contact;

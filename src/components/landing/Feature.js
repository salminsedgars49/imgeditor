import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { FlexBox, FlexItem } from '../flex';
import { Typography } from 'antd';

const { Title } = Typography;


class Feature extends Component {

    render() {
        return (
            <div className="landing-feature">
                <div className="landing-feature__container">
                    <Title>OIE: YOUR ALL-IN-ONE CREATIVE SOLUTION</Title>
                    <p>Watch OIE in action to see how it makes your photo editing, collage making, and graphic design workflow seamless.</p>
                </div>
            </div>
        );
    }
}

export default Feature;

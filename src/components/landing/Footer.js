import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { FlexBox, FlexItem } from '../flex';
import { Typography, Row, Col, Button } from 'antd';

import { AppImages } from '../../assets'


class Footer extends Component {

    render() {

        return (
            <div className="landing-footer">
                <div className="landing-footer__container">
                    <Row gutter={16}>
                        <Col className="gutter-row" span={6}>
                            <FlexBox flex="0 1 auto" alignItems="center">
                                <FlexItem style={{ paddingRight: 20, display: 'flex' }} alignItems="center">
                                    <img src={AppImages.logo} alt="BlackBird Logo" className='rde-app-logo' />
                                    <span style={{ fontSize: 24, fontWeight: 600 }}>OIE</span>
                                </FlexItem>
                            </FlexBox>
                        </Col>
                        <Col className="gutter-row" span={6}>
                            <h3>PRODUCTS</h3>
                            <div><a href="#">Photo Editor</a></div>
                            <div><a href="#">Collage Maker</a></div>
                            <div><a href="#">Graphic Designer</a></div>
                            <div><a href="#">Pricing</a></div>
                        </Col>
                        <Col className="gutter-row" span={6}>
                            <h3>FEATURES</h3>
                            <div><a href="#">Photo Effects</a></div>
                            <div><a href="#">Touch Up</a></div>
                            <div><a href="#">Photo to Art</a></div>
                            <div><a href="#">Text Editor</a></div>
                            <div><a href="#">Essential Features</a></div>
                        </Col>
                        <Col className="gutter-row" span={6}>
                            <h3>RESOURCES</h3>
                            <div><a href="#">Getting Started</a></div>
                            <div><a href="#">User Guides</a></div>
                            <div><a href="#">FAQ</a></div>
                            <div><a href="#">Tutorials</a></div>
                            <div><a href="#">What's New</a></div>
                            <div><a href="#">Case Studies</a></div>
                            <div><a href="#">Explore</a></div>
                        </Col>
                    </Row>
                </div>
                <div className="landing-footer__container">
                    <div className="landing-footer__copyright">
                        &copy; 2020 OIE Inc
                    </div>
                </div>
            </div>
        );
    }
}

export default Footer;

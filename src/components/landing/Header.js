import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { FlexBox, FlexItem } from '../flex';
import { Button } from 'antd';

import { AppImages } from '../../assets'
import { linkConfig } from '../../config'


class Header extends Component {
    static propTypes = {
        currentMenu: PropTypes.string,
        onChangeMenu: PropTypes.func,
    }

    render() {
        return (
            <div className="landing-header">
                <div className="landing-header__container">
                    <FlexBox flexWrap="wrap" flex="1" alignItems="center">
                        <FlexBox flex="0 1 auto" alignItems="center">
                            <FlexItem style={{ paddingRight: 20, display: 'flex' }} alignItems="center">
                                <img src={AppImages.logo} alt="BlackBird Logo" className='rde-app-logo' />
                                <span style={{ fontSize: 24, fontWeight: 600 }}>OIE</span>
                            </FlexItem>
                            <FlexItem>
                                <Button
                                    className="rde-action-btn"
                                    href="#tech"
                                >
                                    Feature
                                </Button>
                            </FlexItem>
                            <FlexItem>
                                <Button
                                    className="rde-action-btn"
                                    href="#showcase"
                                >
                                    Technology
                                </Button>
                            </FlexItem>
                            <FlexItem>
                                <Button
                                    className="rde-action-btn"
                                    href="#contact"
                                >
                                    Contact
                                </Button>
                            </FlexItem>
                        </FlexBox>
                        <FlexBox flex="1 1 auto" justifyContent="flex-end">
                            <FlexItem>
                                <Button
                                    className="rde-action-btn"
                                    href={linkConfig.login}
                                >
                                    Sign In
                                </Button>
                            </FlexItem>
                            <FlexItem>
                                <Button
                                    className="rde-action-btn btn--filled"
                                    href={linkConfig.editor}
                                >
                                    Get Started
                                </Button>
                            </FlexItem>
                        </FlexBox>
                    </FlexBox>
                </div>
            </div>
        );
    }
}

export default Header;

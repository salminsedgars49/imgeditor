import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { FlexBox, FlexItem } from '../flex';
import { Typography, Row, Col, Button } from 'antd';
import { AppImages } from '../../assets'
import { linkConfig } from '../../config'

const { Title } = Typography;


class Showcase extends Component {

    render() {
        return (
            <div className="landing-showcase">
                <div className="landing-showcase__container">
                    <Title>PHOTO EDITING AND GRAPHIC DESIGN. SIMPLIFIED.</Title>

                    <FlexBox alignItems="center" className="landing-showcase__row">
                        <FlexItem alignItems="center">
                            <img src={AppImages.tech_photo_editor_img} alt="tech_photo_editor_img" />
                        </FlexItem>
                        <FlexItem alignItems="center">
                            <div className="landing-showcase__text">
                                <h3>Photo Editor</h3>
                                <div>To put it simply, <b>OIE</b> makes photo editing easy. With our world famous Photo Editor, you can turn photos you like into photos you love! From essential editing tools such as crop, resize, and exposure to our more unique effects like Cartoonizer, Digital Art, and Enhance DLX, it's beyond easy to create great looking photos. Add a little extra flair to your image with hundreds of customizable vector icons and graphic overlays. If picture quotes are your thing, <b>OIE</b>'s Photo Editor has hundreds of free fonts for you to choose from. Finally, a photo editor that's powerful, fun, AND easy to use!
                                </div>
                            </div>
                        </FlexItem>
                    </FlexBox>
                    <FlexBox alignItems="center" className="landing-showcase__row">
                        <FlexItem alignItems="center">
                            <div className="landing-showcase__text">
                                <h3>Collage Maker</h3>
                                <div>Tell a story, share an adventure, or create anything else you want, our Collage Maker is here for you. <b>OIE</b>'s Collage Maker empowers you to easily create breathtaking online photo collages. Simply upload your photos and let our Collage Wizard automatically create a stunning collage for you, or choose from our collection of fully customizable layouts. We even have layouts designed especially for Pinterest, Facebook, Twitter, and Instagram, so your posts are guaranteed to stand out from the crowd.
                                </div>
                            </div>
                        </FlexItem>
                        <FlexItem alignItems="center">
                            <img src={AppImages.tech_collage_img} alt="tech_collage_img" />
                        </FlexItem>
                    </FlexBox>
                    <FlexBox alignItems="center" className="landing-showcase__row">
                        <FlexItem alignItems="center">
                            <img src={AppImages.tech_designer_img} alt="tech_designer_img" />
                        </FlexItem>
                        <FlexItem alignItems="center">
                            <div className="landing-showcase__text">
                                <h3>Graphic Designer</h3>
                                <div>Don't know Photoshop? Now you don't need to. With our Designer Toolset, it's easy to make fully customized graphic designs. Our Graphic Designer empowers you to design like never before. Say ‘Goodbye' to complicated, expensive software and create beautiful online graphic design projects with drag-and-drop simplicity. Take advantage of a huge selection of professionally designed templates, or if you feel like truly embracing your creativity, design a project from scratch with our easy-to-use graphic design tools.
                                </div>
                            </div>
                        </FlexItem>
                    </FlexBox>
                    <FlexBox alignItems="center" className="landing-showcase__row" style={{ paddingTop: 20, paddingBottom: 20 }}>
                        <FlexItem alignItems="center">
                            <Button
                                className="rde-action-btn btn--filled"
                                href={linkConfig.editor}
                            >
                                Get Started
                            </Button>
                        </FlexItem>
                    </FlexBox>
                </div>
            </div>
        );
    }
}

export default Showcase;

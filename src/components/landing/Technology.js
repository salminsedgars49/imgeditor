import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { FlexBox, FlexItem } from '../flex';
import { Typography, Row, Col } from 'antd';
import { AppImages } from '../../assets'

const { Title } = Typography;


class Technology extends Component {

    render() {
        return (
            <div className="landing-tech">
                <div className="landing-tech__container">
                    <Row gutter={16}>
                        <Col className="gutter-row" span={8}>
                            <div className="landing-tech__title">
                                <Title>MOST POPULAR FEATURES</Title>
                                <div>OIE has an amazing collection of tools and features for photo editing, collage making, and graphic design. Here are some favorites.</div>
                            </div>
                            <div className="landing-tech__card">
                                <img src={AppImages.features_batch_process} alt="features_batch_process" />
                                <div className="landing-tech__card__content">
                                    <h2>Batch Edit Photos</h2>
                                    <div>With Batch Processing, you can crop, resize, and enhance multiple photos all at the same time.</div>
                                </div>
                            </div>
                            <div className="landing-tech__card">
                                <img src={AppImages.features_touchup} alt="features_touchup" />
                                <div className="landing-tech__card__content">
                                    <h2>Retouch Portraits</h2>
                                    <div>Perfect portraits and selfies, every time. With our collection of Touch Up tools, we'll have you looking your best in no time.</div>
                                </div>
                            </div>
                        </Col>
                        <Col className="gutter-row" span={8}>
                            <div className="landing-tech__card">
                                <img src={AppImages.features_crop} alt="features_crop" />
                                <div className="landing-tech__card__content">
                                    <h2>Crop and Resize Photos</h2>
                                    <div>With our Photo Editor you can easily crop and resize your images with pixel perfect accuracy.</div>
                                </div>
                            </div>
                            <div className="landing-tech__card">
                                <img src={AppImages.features_transpbg} alt="features_transpbg" />
                                <div className="landing-tech__card__content">
                                    <h2>Create Transparent Backgrounds</h2>
                                    <div>Our Cutout tool makes it easy to remove backgrounds from your images. It's perfect for product photography, logos, swapping backgrounds, and so much more.</div>
                                </div>
                            </div>
                        </Col>
                        <Col className="gutter-row" span={8}>
                            <div className="landing-tech__title" style={{ visibility: 'hidden' }}>
                                <Title>MOST POPULAR FEATURES</Title>
                                <div>OIE has an amazing collection of tools and features for photo editing, collage making, and graphic design. Here are some favorites.</div>
                            </div>
                            <div className="landing-tech__card">
                                <img src={AppImages.features_photoart} alt="features_photoart" />
                                <div className="landing-tech__card__content">
                                    <h2>Turn Photos To Art</h2>
                                    <div>Give your photos a truly unique look. Our Artsy filters will make your photos look like classic style paintings, sketches, cartoons, and more!</div>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

export default Technology;

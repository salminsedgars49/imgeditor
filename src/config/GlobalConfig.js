import { AppImages } from '../assets'

export const imgSizeLimit = 5

export const startImages = [
    {
        uid: '1',
        name: 'image.png',
        url: AppImages.start_home,
    },
    {
        uid: '2',
        name: 'image.png',
        url: AppImages.start_street,
    },
    {
        uid: '3',
        name: 'image.png',
        url: AppImages.start_landscape,
    }
]

export const galleryImages = [
    AppImages.gallery_1,
    AppImages.gallery_2,
    AppImages.gallery_3,
    AppImages.gallery_4,
    AppImages.gallery_5,
    AppImages.gallery_6
];

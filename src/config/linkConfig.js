const link = {
    editor: '/#/create',
    login: '/#/login',
    register: '/#/register',
    home: '#',
}

export default link

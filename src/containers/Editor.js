import React, { Component } from 'react';
import { Helmet } from 'react-helmet';

import ImageMapEditor from '../components/imagemap/ImageMapEditor';
import Header from './Header';

class Editor extends Component {
    state = {
        current: 'imagemap',
    }

    onChangeMenu = ({ key }) => {
        this.setState({
            current: key,
        });
    }

    render() {
        const { current } = this.state;
        return (
            <div className="rde-main">
                <Helmet>
                    <meta charSet="utf-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                    <meta name="description" content="OIE - Elegant Online Image Editor" />
                    <link rel="manifest" href="./manifest.json" />
                    <link rel="shortcut icon" href="./favicon.ico" />
                    <link rel="stylesheet" href="https://fonts.googleapis.com/earlyaccess/notosanskr.css" />
                    <title>OIE - Elegant Online Image Editor</title>
                    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js" />
                </Helmet>
                <div className="rde-title">
                    <Header onChangeMenu={this.onChangeMenu} current={current} />
                </div>
                <div className="rde-content">
                    <ImageMapEditor />
                </div>
            </div>
        );
    }
}

export default Editor;

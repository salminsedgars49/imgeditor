import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { FlexBox, FlexItem } from '../components/flex';
import { AppImages } from '../assets'
import { Button } from 'antd';
import { linkConfig } from '../config'


class Header extends Component {
    static propTypes = {
        currentMenu: PropTypes.string,
        onChangeMenu: PropTypes.func,
    }

    componentDidMount() {
        if (window) {
            (window.adsbygoogle = window.adsbygoogle || []).push({});
        }
    }

    render() {
        return (
            <FlexBox style={{ backgroundColor: '#373842', boxShadow: '0 0 0.5rem rgba(0,0,0,.8)' }} flexWrap="wrap" flex="1" alignItems="center">
                <FlexBox style={{ paddingLeft: 20 }} flex="0 1 auto" alignItems="center">
                    <img src={AppImages.logo} alt="Logo" className='rde-app-logo' />
                    <span style={{ color: '#fff', fontSize: 24, fontWeight: 600 }}>OIE</span>
                </FlexBox>
                <FlexBox style={{ paddingRight: 20 }} flex="1 1 auto" justifyContent="flex-end">
                    <FlexItem>
                        <Button
                            className="rde-action-btn menu-item"
                            href={linkConfig.login}
                        >
                            Sign In
                        </Button>
                    </FlexItem>
                    <FlexItem>
                        <Button
                            className="rde-action-btn menu-item"
                            href={linkConfig.register}
                        >
                            Register
                        </Button>
                    </FlexItem>
                </FlexBox>
            </FlexBox>
        );
    }
}

export default Header;

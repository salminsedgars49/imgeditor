import React, { Component } from 'react';
import Header from '../components/landing/Header';
import Banner from '../components/landing/Banner';
import Feature from '../components/landing/Feature';
import Technology from '../components/landing/Technology';
import Showcase from '../components/landing/Showcase';
import Contact from '../components/landing/Contact';
import Footer from '../components/landing/Footer';


class Landing extends Component {
    state = {
        current: 'imagemap',
    }

    onChangeMenu = ({ key }) => {
        this.setState({
            current: key,
        });
    }


    render() {
        const { current } = this.state;

        return (
            <div className="landing">
                <Header onChangeMenu={this.onChangeMenu} current={current} />
                <section id="banner">
                    <Banner />
                </section>
                {/*<section id="feature">
                    <Feature />
                </section>*/}
                <section id="tech">
                    <Technology />
                </section>
                <section id="showcase">
                    <Showcase />
                </section>
                <section id="contact">
                    <Contact />
                </section>
                <section id="footer">
                    <Footer />
                </section>
            </div>
        );
    }
}

export default Landing;

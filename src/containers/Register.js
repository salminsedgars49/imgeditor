import React, { Component } from 'react';
import { Form, Input, Button, Checkbox, Row, Col } from 'antd';
import { AppImages } from '../assets'
import { linkConfig } from '../config'
import { SearchOutlined } from '@ant-design/icons';


class Register extends Component {

    render() {

        const welcomeStyle = {
            backgroundImage: `url(${AppImages.tech_photo_editor_img})`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat'
        }

        return (
            <div className="auth">
                <div className="auth__container">
                    <Row type="flex">
                        <Col sm={24} md={10} >
                            <div className="auth__form-wrapper">
                                <div className="auth__form-logo">
                                    <img src={AppImages.logo} alt="Logo" />
                                    <h1>OIE</h1>
                                </div>
                                <div className="auth__form">
                                    <Form
                                        name="basic"
                                        initialValues={{ remember: true }}
                                    >
                                        <Form.Item
                                            name="name"
                                            rules={[{ required: true, message: 'Please input your name!' }]}
                                        >
                                            <Input placeholder="Username *" />
                                        </Form.Item>

                                        <Form.Item
                                            name="email"
                                            rules={[{ required: true, message: 'Please input your email!' }]}
                                        >
                                            <Input placeholder="Email Address *" />
                                        </Form.Item>

                                        <Form.Item
                                            name="password"
                                            rules={[{ required: true, message: 'Please input your password!' }]}
                                        >
                                            <Input.Password placeholder="Password *" />
                                        </Form.Item>

                                        <Form.Item name="remember" valuePropName="checked">
                                            <Checkbox>Agree with <a href="#">Terms & Condition</a></Checkbox>
                                        </Form.Item>

                                        <Form.Item>
                                            <Button type="primary" htmlType="submit">
                                                REGISTER
                                            </Button>
                                        </Form.Item>
                                    </Form>
                                </div>
                                <div className="auth__switching">
                                    <div>Already have an account? <a href={linkConfig.login}>Sign in</a></div>
                                </div>
                            </div>
                        </Col>
                        <Col sm={24} md={14} className="welcome" style={welcomeStyle}>
                            <div className="welcome__content">
                                <div className="welcome__text">
                                    <h1>YOUR ALL-IN-ONE<br />CREATIVE SOLUTION</h1>
                                    <h3>Watch <b>OIE</b> in action to see<br />how it makes your photo editing,<br/>collage making, and graphic design workflow seamless.</h3>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

export default Register;

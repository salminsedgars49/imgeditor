import React from 'react';
import { HashRouter as Router, Route } from "react-router-dom";
// import { BrowserRouter, Switch, Route } from 'react-router-dom';
import About from './About';
import Landing from './Landing';
import Editor from './Editor';
import Login from './Login';
import Register from './Register';


class App extends React.Component {
    render() {
        return (
            <Router>
                <Route path="/" exact component={Landing} />
                <Route
                    path="/create" component={Editor}
                />
                <Route path="/login" component={Login} />
                <Route path="/register" component={Register} />
                <Route path="/about" component={About} />
            </Router>
        );
    }
}

export default App;

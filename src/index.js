import React from 'react';
import ReactDom from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { LocaleProvider } from 'antd';
import koKR from 'antd/lib/locale-provider/ko_KR';
import enUS from 'antd/lib/locale-provider/en_US';

import App from './containers';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from "react-redux";
import store from "./store"

import { i18nClient } from './i18n';

const antResources = {
    en: enUS,
    'en-US': enUS,
};

const root = document.createElement('div');
root.id = 'root';
document.body.appendChild(root);

const render = (Component) => {
    const rootElement = document.getElementById('root');
    ReactDom.render(
        <Provider store={store}>
            <AppContainer>
                <LocaleProvider locale={antResources[i18nClient.language]}>
                    <Component />
                </LocaleProvider>
            </AppContainer>
        </Provider>,
        rootElement,
    );
};

render(App);

if (module.hot) {
    module.hot.accept('./containers', () => {
        render(App);
    });
}

//registerServiceWorker();

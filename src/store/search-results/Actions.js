import { UPDATE_RESULTS } from "./ActionTypes"

export const updateResults = data => {
    return {
        type: UPDATE_RESULTS,
        payload: data
    }
}

import { UPDATE_RESULTS } from "./ActionTypes"

const initialState = {
    urls: [],
    searchValue: '',
    loaded: false,
    loading: false,
    downloading: false,
    checkList: [],
    startSession: false,
}

export const searchResults = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_RESULTS:
            return { ...state, ...action.payload }
        default:
            return state
    }
}

export const getResults = (state) => {
    return state.searchResults
}

import { API_BASE_URL, convertFormData } from '../config/GlobalConfig'

const AppUtils = {
    searchWithText: async function(data) {
        return fetch(API_BASE_URL + '?q=' + data, {
            method: 'GET',
            headers: {
                Accept: 'application/json'
            }
        }).then(response => response.json())
    },
    searchWithImageUrl: async function(data) {
        return fetch(API_BASE_URL + '?url=' + data, {
            method: 'GET',
            headers: {
                Accept: 'application/json'
            }
        }).then(response => response.json())
    },
    searchWithImageHash: async function(data) {
        return fetch(API_BASE_URL + '/' + data, {
            method: 'GET',
            headers: {
                Accept: 'application/json'
            }
        }).then(response => response.json())
    },
    uploadImage: async function(data) {
        return fetch(API_BASE_URL, {
            method: 'POST',
            headers: {
                // 'Content-Type': 'multipart/form-data',
                // Accept: 'application/json'
            },
            body: await convertFormData(data)
        }).then(response => response.json())
    },
    downImages: async function(data) {
        return fetch(API_BASE_URL, {
            method: 'POST',
            headers: {
                // 'Content-Type': 'multipart/form-data',
                Accept: 'application/json'
            },
            body: await convertFormData(data)
        }).then(response => response.json())
    }
}

export default AppUtils
